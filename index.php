<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<?php
$dsn = "mysql:host=localhost;dbname=videoclub;port=3306;charset=utf8";
$userNameDB = "eleve";
$password = "bonjour";


try {
    $dbh = new PDO($dsn, $userNameDB, $password);
} catch (PDOException $e) { // Retourne une erreur
    var_dump($e);
}

var_dump($_GET);
if(!empty($_GET['ajouter'])) {
    $filmAAjouter = $_GET['nom_film'];

    $ajoutFilm = $dbh->prepare("INSERT INTO film(titre) VALUES (:titre) ");
    $ajoutFilm->execute([
            'titre' => $filmAAjouter,
    ]);
}

if(!empty($_GET['supprimer'])){
    $filmASuppr = $_GET['nom_film_a_supprimer'];
//    var_dump($filmASuppr);
    $supprFilm = $dbh->prepare("DELETE FROM film WHERE titre = :titre");
    $supprFilm->execute([
            'titre' => $filmASuppr,
    ]);
}

if(!empty($_GET['modifier'])){
    $filmAModif = $_GET['nom_film_a_modifier'];
    $nouveauFilm = $_GET['nouveau_nom_film'];

    $modifFilm = $dbh->prepare("UPDATE film SET titre = :nouveauTitre WHERE titre = :ancienTitre");
    $modifFilm->execute([
            'ancienTitre' => $filmAModif,
            'nouveauTitre' => $nouveauFilm,
    ]);
}

$films = $dbh->prepare("SELECT titre FROM film");
$films->execute();
echo "<h1> Films disponibles </h1>";
while ($film = $films->fetch(PDO::FETCH_ASSOC)){
    echo "<div>";
    echo $film['titre'];
    echo"</div>";
}
?>
<br>
<h2>Ajouter un nouveau film</h2>
<form action="index.php" method="GET">
    <label for="nom_film">Entrez le nom du film</label><br>
    <input type="text" name="nom_film">
    <input type="submit" value="Ajouter le film" name="ajouter">


</form>

<h2>Supprimer un film</h2>
<form action="index.php" method="get">
    <select name="nom_film_a_supprimer" id="nom_film">
        <?php
        $films->execute();
        while($film = $films->fetch(PDO::FETCH_ASSOC)){?>
            <option value="<?php
            echo $film['titre'];?>">
            <?php echo $film['titre'];?>
            </option><?php
        }?>
    </select>

    <input type='submit' value='Supprimer le film' name="supprimer">
</form>

<h2>Modifier un film existant</h2>
<form action="index.php" method="get">
    <p>Film à modifier</p>
    <select name="nom_film_a_modifier" id="nom_film">
        <?php
        $films->execute();
        while($film = $films->fetch(PDO::FETCH_ASSOC)){?>
            <option value="<?php
            echo $film['titre'];?>">
            <?php echo$film['titre']
            ?></option><?php
        }?>
    </select>
    <br><br>
    <label for="nouveau_nom_film">Entrez le nouveau nom du film</label><br><br>
    <input type="text" name="nouveau_nom_film" id="nouveau_nom_film">
    <input type='submit' value='Modifier le nom du film' name="modifier">
</form>
</body>
</html>

